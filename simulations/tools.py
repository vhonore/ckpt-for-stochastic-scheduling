from copy import *
from cmath import *
import scipy
import os


import distri
from distri import *
import settings




########################## START Application Time ##########################


# Get recovery time of reservation k in schedule
def Rk(schedule,k):
	if(k<=1):
		return 0
	tmp = 1
	for i in range(1,k):
		tmp = tmp * (1-schedule[i][1])
	return ((1-tmp)*settings.R)
	


# Get actual length of reservation k in schedule
def Tk(schedule,k):
	tk = schedule[k][0]
	if (k==0):
		return schedule[k][0]
	max_ti = schedule[0][0]
	for i in range(1,k):
		if (schedule[i][1]):
			max_ti = schedule[i][0]
	return tk - max_ti



# Get checkpointing time of reservation k in schedule
def Ck(schedule,k):
	assert(schedule[k][1]==1 or schedule[k][1]==0)
	if (k==0 or k==(len(schedule)-1)):
		return 0
	return schedule[k][1]*getattr(settings,"C")



# Get the actual cost of reservation k in schedule
def Wk(schedule,k):
	if(k==0):
		return 0
	return (Rk(schedule,k) + Tk(schedule,k) + Ck(schedule,k))








# Computes the expeted cost of sequence of reservation schedule associated to distribution distrib, using waiting time function waitingTimeFunction
# schedule = ((t1,d1),(t2,d2),(t3,d3)...) the list of reservations and associated checkpointing decisions
# distrib = the considered distrib
# waitingTimeFunction = a pointer to the waiting time function alpha*t + gamma
def cost_computation_stochastic(distrib,schedule,waitingTimeFunction):

	mean = getattr(distri,"mean_"+distrib.lower())	

	# init for i=0 and i=1
	cost = mean*settings.beta + waitingTimeFunction(schedule[1][0]+schedule[1][1]*settings.C)

	# main loop
	i = 2
	while (i<len(schedule)):	
		cost += (waitingTimeFunction(Wk(schedule,i))+settings.beta*(Rk(schedule,i)+(1-schedule[i-1][1])*Tk(schedule,i-1) + Ck(schedule,i-1)))*globals()["CDFr_"+distrib.lower()](schedule[i-1][0])
		#cost += settings.beta*((1-schedule[i][1])*Tk(schedule,i)+Ck(schedule,i))*(globals()["CDFr_"+distrib.lower()](schedule[i][0]))
		i+=1
	
	return cost





########################## END Application Time ##########################














########################## TOOLS TO DISCRETIZE A CONTINUOUS PROBABILITY DISTRIB ##########################


# Discretization scheme used in the different algorithms
def discretization_proba(distrib, numberChunks):

	assert(numberChunks)
	result = [[],[]] # structure to represent couples ti and pi
	max_distrib=0

	a = getattr(distri, "min_"+distrib.lower())
	b = getattr(settings, "max_"+distrib.lower())
	if(distrib in globals()["list_unbounded_distrib"]):
		max_distrib=1 # used to normalize when we truncate unbounded distrib
		

	ti1=a
	cpt,f=0.0,0.0
	for i in range(0,numberChunks+1): 	# in this discretization, pi are the same
		t = a+i*((b-a)/numberChunks)
		result[0].append(t)
		if(i):
			f = globals()["CDF_"+distrib.lower()](t) - globals()["CDF_"+distrib.lower()](ti1)
		else:
			f = globals()["CDF_"+distrib.lower()](t)
		ti1=deepcopy(t)
		if (max_distrib):
			f = f/(globals()["CDF_"+distrib.lower()](b)-globals()["CDF_"+distrib.lower()](a))
		cpt+=f
		result[1].append(f)
	print("sum adjusted probability = "+str(cpt))
	# print(result)
	return result





########################## END TOOLS TO DISCRETIZE A CONTINUOUS PROBABILITY DISTRIB ##########################