import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib import rc
from scipy.stats import lognorm
from scipy.stats import gamma as gamma_d
rc('text', usetex=True)

from matplotlib.ticker import NullFormatter 
from matplotlib.ticker import ScalarFormatter
from matplotlib.ticker import FixedLocator


from copy import *
import numpy as np
from numpy import log as ln
from cmath import *
import scipy
import json
import time


import pickle
from settings import *
import distri
from tools import *
from algorithms import *



path = "results/"



### INIT DISTRIB AND SETTINGS ###
init_settings()
init_distributions()
### END INIT DISTRIB AND SETTINGS ###








## STARTING SIMULATION PROCESS ##


print("\n\n\n!!!!!! STARTING SIMULATIONS !!!!!!\n\n\n")










####################### START simulation for Algorithm 1 (Figure 6 in paper, Figure 7,8 in research report) #######################
if(settings.simulation_FPTAS_bounded_plot_data or settings.simulation_plot_FPTAS_bounded):

	# tested values of R=C
	candidate_R_C = [360,1800,3600]

	# fix the desired value for beta (HPC cost function is 1, cloud cost function is 0)
	settings.beta = 0.0

	

	for cost in candidate_R_C:
		
		# Path to save results
		path_ = path+"beta="+str(int(settings.beta))+"/FPTAS_C=R="+str(cost)+"/"


		print("C = R = "+str(cost)+"\n\n")
		settings.R = settings.C = deepcopy(cost)/3600


		copy = deepcopy(settings.epsilon)
		settings.epsilon = [2,1.9,1.8,1.7,1.6,1.5,1.4,1.3,1.2,1.1,1,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1]
		print(settings.epsilon)
		# save perf for each algo and each eps
		r = []
		# save exec time
		t = []

		# GENERATE DATA
		if (settings.simulation_FPTAS_bounded_plot_data):

			n=res=sched=n0=n_ac=res_ac=sched_ac=n0_ac=0
			distri.list_distrib=["Exponential","Weibull","Gamma","Lognormal","Pareto","TruncatedNormal","Uniform","Beta","BoundedPareto"]
			print(distri.list_distrib)
			for d in distri.list_distrib:
				print("## distribution "+str(d)+ "##\n")
				mean_dist = getattr(distri, "mean_"+d.lower())


				# r is to store results for plots
				# allckpt is to sort the performance in allckpt version
				# nockpt is to sort the performance in nockpt version
				# r,allckpt,nockpt,perf = [],[],[],[]
				perf = []
				exectime = []
				# simulation for bounded FPTAS
				for eps in getattr(settings,"epsilon"):
					print("\n  ** precision="+str(eps)+" **")
					

					# FPTAS bounded
					print("		 - FPTAS for distribution "+str(d))
					
					start = time.time()
					n,res,sched = FPTAS_bounded(d, settings.linear,eps)
					end = time.time()
					exect = end-start
					print(res)
					print("		 ... done")
			

					# print the normalized results
					print("    --> res = "+str(res))
					print("    --> res (by omniscient) = "+str(res/mean_dist))
					print("    --> exec time (sec) = "+str(exect))
					perf.append(res)
					exectime.append(exect)
				r.append(perf)
				t.append(exectime)
				print("## done distribution "+str(d)+" ##\n\n\n")

			with open(path_+"perf_plot_"+str(cost)+".txt", 'wb') as filehandle:
				# store the data as binary data stream
				pickle.dump(r, filehandle)

			with open(path_+"perf_time_"+str(cost)+".txt", 'wb') as filehandle:
				# store the data as binary data stream
				pickle.dump(t, filehandle)


		# PLOT DATA
		if (settings.simulation_plot_FPTAS_bounded):


			
			
			markers = ["x","s","*",">","<","P","X","D","h"]
			colors = ['black','red','dodgerblue','green','blue','magenta','saddlebrown','darkmagenta','darkorange']
			time = []
			perf = []
			with open(path_+"perf_plot_"+str(cost)+".txt", 'rb') as filehandle:
				# read the data as binary data stream
				perf = pickle.load(filehandle)
			with open(path_+"perf_time_"+str(cost)+".txt", 'rb') as filehandle:
				# read the data as binary data stream
				time = pickle.load(filehandle)


			

			print("\n\nGenerating plots...")

			##### perf #####
			
		
			plt.figure()
			ax = plt.gca()
			maxi = -1

			print(len(perf))
			for i in range(0,len(perf)):	
				norm = perf[i][t-1]
				maxi = max(maxi,max(perf[i][j]/norm for j in range (0,t-1)))
				marksize=7
				if (i==0):
					marksize=8
				line, = ax.plot([settings.epsilon[j] for j in range(0,t-1)],[perf[i][j]/norm for j in range (0,t-1)],colors[i], marker=markers[i], lw=3, alpha=0.6, label=r''+distri.list_distrib[i],markersize=marksize)
			line, = ax.plot([settings.epsilon[j] for j in range(0,t-1)],[1+settings.epsilon[j] for j in range (0,t-1)],color='dimgray', linestyle="--", lw=3, alpha=0.6, label=r''+"$y=1+\\varepsilon$")
			
			
			for axis in [ax.yaxis]:
					axis.set_minor_formatter(NullFormatter())
			ax.yaxis.set_major_formatter(ScalarFormatter())
			
			plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left",
	                mode="expand", borderaxespad=0, ncol=3,fontsize=13.5)
			ax.set_xlabel(r''+'$\\varepsilon$',fontsize=23)
			ax.set_ylabel(r''+'{\sc DPC}$(X,\\varepsilon)$/{\sc DPC}$(X,0.1)$',fontsize=17.5)
			ax.set_xlim([0.2*0.75,max(settings.epsilon)*1.05])
			ax.tick_params(axis='both', which='major', labelsize=16)
			

			
			ax.xaxis.set_major_locator(MultipleLocator(0.2))
			# ax.xaxis.set_minor_locator(MultipleLocator(0.04))
			ax.yaxis.set_major_locator(MultipleLocator(1))
			ax.yaxis.set_minor_locator(MultipleLocator(0.2))
			plt.tight_layout()
			plt.savefig(path_+"perf_cost.pdf",orientation='landscape')
			plt.close()


			##### execution time #####

			plt.figure()
			maxi = -1
			ax = plt.gca()
			
				
			for i in range(0,len(time)):	
				maxi = max(maxi,max(time[i]))
				line, = ax.plot(settings.epsilon,time[i],colors[i], marker=markers[i], lw=3, alpha=0.6, label=r''+distri.list_distrib[i])
			
			plt.yscale('log')
			plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left",
	                mode="expand", borderaxespad=0, ncol=3,fontsize=13)
			ax.set_xlabel('Epsilon',fontsize=18)
			ax.set_ylabel('Exec. time in seconds (logscale)',fontsize=18)
			ax.tick_params(axis='both', which='major', labelsize=12)
			ax.xaxis.set_major_locator(MultipleLocator(0.2))
			plt.tight_layout()
			plt.savefig(path_+"perf_time.pdf",orientation='landscape')

			
			plt.close()

	settings.beta = 0.0
####################### END simulation Algorithm 1 #######################









####################### START simulation for periodic algorithms (Algorithm 2), generates Table II of the paper, Tables II,III,IV,V of the research report  #######################
if (settings.simulation_periodic):

	# tested values of R=C
	candidate_R_C = [360,1800,3600]

	# fix the desired value for beta (HPC cost function is 1, cloud cost function is 0)
	settings.beta = 0.0

	for cost in candidate_R_C:

		# Path to save results
		path_ = path+"beta="+str(int(settings.beta))+"/periodic_"+str(cost)+"/"

		settings.R = settings.C = deepcopy(cost)/3600
 
		# file for the table in the paper
		ind = [1,200,400,600,800,1000] # !!! : range exclude the last one
		tab_periodic = open(path_+'tab_periodic_'+str(cost)+'.tex','w')
		tab_periodic.write("\\begin{tabular}{|c||")
		for _ in range(0,len(ind)+1):
			tab_periodic.write("c|")
		tab_periodic.write("|c||")
		for _ in range(0,len(ind)+1):
			tab_periodic.write("c|")
		tab_periodic.write("}\n\\hline\n")
		tab_periodic.write("\\multirow{2}{*}{Distribution} & \\multicolumn{"+str(len(ind)+1)+"}{c||}{\\algoPeriodic} & \\multicolumn{"+str(len(ind)+1)+"}{c|}{\\noCheckpointPeriodic}\\\\ \n")
		tab_periodic.write("\cline{2-"+str(2*len(ind)+3)+"}\n & Best $\\tau$ &")
		for k in range(0,len(ind)):
				tab_periodic.write("$\\tau="+str(ind[k])+"$ & ")
		tab_periodic.write("Best $\\tau$ &")
		for k in range(0,len(ind)-1):
				tab_periodic.write("$\\tau="+str(ind[k])+"$ & ")
		tab_periodic.write("$\\tau="+str(ind[len(ind)-1])+"$ \\\\ \n \hline \n")

		


		outfile = open(path_+"data_periodic_"+str(cost)+".txt","w")
		for p in settings.periods:
			outfile.write(str(p)+",")
		outfile.write("\n\n")

		result_ckpt = []
		result_nockpt = []

		#### Periodic simulations
		print("Start Periodic \n\n")
		for d in distri.list_distrib:
			print("     Starting "+d+"\n")
			
			# get result for FPTAS
			mean_dist = getattr(distri, "mean_"+d.lower())
			

			eps=0.7
			if (d=="Pareto"):
				eps = 0.1
			elif(d=="Weibull"):	
				eps=0.3


			n_FPTAS,res_FPTAS,schedule = FPTAS_bounded(d, settings.linear,eps) # get result of FPTAS



			# With checkpoints
			min_= float('inf')
			best_T =  0
			r = []
			for p in settings.periods:
				res,sched = periodic(d, settings.linear, p)
				r.append(deepcopy(res))
				if (res<min_):
					min_=deepcopy(res)
					best_T = deepcopy(p)
			result_ckpt.append(r)

			tab_periodic.write(str(d)+" & ")
			tab_periodic.write(str(best_T)+" ("+str(format(min_/res_FPTAS,'.2f'))+") & ")
			# print(ind)
			for k in ind:
				assert(k-1<len(settings.periods))
				tab_periodic.write("$"+str(format(r[k-1]/res_FPTAS,'.2f'))+"$ & ")




			# Without checkpoints
			min_= float('inf')
			best_T =  0
			best_sched = []
			r = []
			for p in settings.periods:
				res,sched = periodic(d, settings.linear, p, False)
				r.append(deepcopy(res))
				if (res<min_):
					min_=deepcopy(res)
					best_T = deepcopy(p)
					best_sched = deepcopy(sched)
					# print(best_sched)
			result_nockpt.append(r)
			
			tab_periodic.write(str(best_T)+" ("+str(format(min_/res_FPTAS,'.2f'))+") & ")
			for k in range(0,len(ind)-1):
				assert(ind[k]-1<len(settings.periods))
				tab_periodic.write("$"+str(format(r[ind[k]-1]/res_FPTAS,'.2f'))+"$ & ")

			tab_periodic.write("$"+str(format(r[ind[len(ind)-1]-1]/res_FPTAS,'.2f'))+"$ \\\\ \n\\hline \n ")
			print("     Done "+d+"\n")
		print("\n\nEnd Periodic\n\n")
			




		# writing result_ckpt.txt
		for i in range (0,len(result_ckpt)-1):
			for j in range (0,len(result_ckpt[i])-1):
				outfile.write(str(result_ckpt[i][j])+",")
			outfile.write(str(result_ckpt[i][len(result_ckpt[i])-1]))
			outfile.write(";\n\n")
		for j in range(0,len(result_ckpt[0])-1):
			outfile.write(str(result_ckpt[len(result_ckpt)-1][j])+",")
		outfile.write(str(result_ckpt[len(result_ckpt)-1][len(result_ckpt[0])-1]))



		print("\n\n\n\n#\n\n\n\n")



		# writing result_nockpt.txt
		for i in range (0,len(result_nockpt)-1):
			for j in range (0,len(result_nockpt[i])-1):
				outfile.write(str(result_nockpt[i][j])+",")
			outfile.write(str(result_nockpt[i][len(result_nockpt[i])-1]))
			outfile.write(";\n\n")
		for j in range(0,len(result_nockpt[0])-1):
			outfile.write(str(result_nockpt[len(result_nockpt)-1][j])+",")
		outfile.write(str(result_nockpt[len(result_nockpt)-1][len(result_nockpt[0])-1]))



		# write the end of tabular
		tab_periodic.write("\\end{tabular} \n")

		# close python streams
		tab_periodic.close()	
		outfile.close()

	settings.beta = 0.0
####################### END simulation Algorithm 2 #######################










####################### START Simulation for the study of the impact of the value of R and C. Generates Fig5 of the paper, Figures 5-6 of the research report #######################
if (settings.simulation_generate_checkpointing_impact_data or settings.simulation_plot_checkpointing_impact):
	liste_distrib = ["Exponential","Gamma","TruncatedNormal","Uniform","Beta","BoundedPareto","Lognormal","Weibull","Pareto"]


	candidate_R_C = [(60+i)/3600 for i in range(0,3540+1,60)]
	candidate_R_C = [candidate_R_C[i] for i in range(0,len(candidate_R_C),3)]
	candidate_R_C.append(1)
	
	eps = 0

	settings.beta=0.0

	# generating the data
	if (settings.simulation_generate_checkpointing_impact_data):
		for d in liste_distrib:
			allckpt,nockpt,perf,period_ckpt,period_nockpt = [],[],[],[],[]
			print("## distribution "+str(d)+ "##\n")
			mean_dist = getattr(distri, "mean_"+d.lower())

			eps=0.7
			if (d=="Pareto"):
				eps = 0.1
			elif(d=="Weibull"):	
				eps=0.3
			
			path_ = path+"beta="+str(int(settings.beta))+"/R_C_impact/"
			
			# simulation for bounded FPTAS
			for R_C in candidate_R_C:
				print("\n  ** precision="+str(eps)+" **")
				
				settings.R=settings.C=R_C
				print(settings.R)

				# FPTAS bounded
				print("		 - FPTAS for distribution "+str(d))
				n,res,sched = FPTAS_bounded(d, settings.linear,eps)
				print(res)
				print("		 ... done")
				
				# FPTAS nockpt
				print("		 - No checkpoint result for distribution "+str(d))
				n,nocheckpt,liste = FPTAS_bounded(d, settings.linear,eps,False,True)
				print(nocheckpt)
				print("		 ... done")
				
				# FPTAS allckpt
				print("		 - FPTAS for distribution "+str(d)+" in allCheckpoint")
				n,res_ac,sched_ac = FPTAS_bounded(d, settings.linear,eps,True,False)
				print(res_ac)
				print("		 ... done")

				# print the normalized results
				print("    --> res = "+str(res))
				print("    --> res (by NO-Chkpt) = "+str(res/nocheckpt))
				print("    --> res (by omniscient) = "+str(res/mean_dist))
				print("    --> res (by ALL-Chkpt) = "+str(res/res_ac)+"\n\n")

				nockpt.append(nocheckpt)
				allckpt.append(res_ac)
				perf.append(res)

				## all ckpt
				min_= float('inf')
				best_T =  0
				for p in settings.periods:
					res,sched = periodic(d, settings.linear, p)
					if (res<min_):
						min_=deepcopy(res)
						best_T = deepcopy(p)
				period_ckpt.append(min_)
				# print(best_T)
				
				### nockpt
				min_= float('inf')
				best_T =  0
				for p in settings.periods:
					res,sched = periodic(d, settings.linear, p, False)
					if (res<min_):
						min_=deepcopy(res)
						best_T = deepcopy(p)
				# print(best_T)
				period_nockpt.append(min_)

					
				# period_ckpt.append(res1)
				# period_nockpt.append(res2)


			liste = []
			liste.append(perf)
			liste.append(allckpt)
			liste.append(nockpt)
			liste.append(period_ckpt)
			liste.append(period_nockpt)
			
			with open(path_+"plot_R_C_"+d.lower()+".txt", 'wb') as filehandle:
				# store the data as binary data stream
				pickle.dump(liste, filehandle)



	# plotting the impact of R and C in FPTAS bounded
	if (settings.simulation_plot_checkpointing_impact):
		for d in liste_distrib:
			print("## distribution "+str(d)+ "##\n")
			mean_dist = getattr(distri, "mean_"+d.lower())

			candidate_R_C = [(60+i)/3600 for i in range(0,3540+1,60)]
			candidate_R_C = [candidate_R_C[i] for i in range(0,len(candidate_R_C),3)]
			candidate_R_C.append(1)
			if (d=="Pareto" and settings.beta==1.0):
				candidate_R_C = [0.016666666666666666, 0.1, 0.26666666666666666, 0.43333333333333335, 0.5166666666666667, 0.6, 0.6833333333333333, 0.7666666666666667, 0.88,1]
		
			allckpt,nockpt,perf,periodic_ckpt,periodic_nockpt = [],[],[],[],[]
			
			mean_dist = getattr(distri, "mean_"+d.lower())
				

			# get data
			res = []
			with open(path_+"plot_R_C_"+d.lower()+".txt", 'rb') as filehandle:
				# read the data as binary data stream
				res = pickle.load(filehandle)

			perf = res[0]

			allckpt = res[1]
			allckpt = [allckpt[i]/perf[i] for i in range(0,len(allckpt))]

			nockpt = res[2]
			nockpt = [nockpt[i]/perf[i] for i in range(0,len(nockpt))]

			periodic_ckpt = res[3]
			periodic_ckpt = [periodic_ckpt[i]/perf[i] for i in range(0,len(periodic_ckpt))]

			periodic_nockpt = res[4]
			periodic_nockpt = [periodic_nockpt[i]/perf[i] for i in range(0,len(periodic_nockpt))]
		
			
			colors = ['black','darkorange','red','green','blue']

			plt.figure()
			plt.plot([r*3600 for r in candidate_R_C], [1.0 for e in perf] , 'black', marker="x",lw=4, alpha=0.6,markersize=9)#, label=r''+'{\sc Dyn-Prog-Count}')
			plt.plot([r*3600 for r in candidate_R_C], allckpt , 'darkorange', marker="s",lw=4, alpha=0.6, label=r''+'{\sc All-Ckpt}',markersize=7)
			plt.plot([r*3600 for r in candidate_R_C], nockpt, 'red', marker="*",lw=4, alpha=0.6, label=r''+'{\sc No-Ckpt}',markersize=7)
			plt.plot([r*3600 for r in candidate_R_C], periodic_ckpt, 'green', marker="<",lw=4, alpha=0.6, label=r''+'{\sc All-Ckpt-Per}',markersize=7)
			plt.plot([r*3600 for r in candidate_R_C], periodic_nockpt, 'blue', marker=">",lw=4, alpha=0.6, label=r''+'{\sc No-Ckpt-Per}',markersize=7)
			plt.legend(loc='best', frameon=False, fontsize=14)
			plt.xlabel('Cost of C and R (seconds)',fontsize=22)
			plt.ylabel(r''+'{\sc Algo}/{\sc DPC}(X,0.1)',fontsize=22)

			axes = plt.gca()
			if (d=="Exponential"):
				axes.set_xlim([0,3700])
				axes.set_ylim([0.8,1.8])
				axes.xaxis.set_major_locator(MultipleLocator(1000))
				axes.xaxis.set_minor_locator(MultipleLocator(200))
				axes.yaxis.set_major_locator(MultipleLocator(0.2))
				axes.yaxis.set_minor_locator(MultipleLocator(0.04))
				plt.legend(loc='best', frameon=False, fontsize=16)
			if (d=="Beta"):
				axes.set_ylim([0.95,1.225])
				axes.set_xlim([0,3700])
				axes.xaxis.set_major_locator(MultipleLocator(1000))
				axes.xaxis.set_minor_locator(MultipleLocator(200))
				axes.yaxis.set_major_locator(MultipleLocator(0.1))
				axes.yaxis.set_minor_locator(MultipleLocator(0.02))
				plt.legend(loc='best', frameon=False, fontsize=16)
			if (d=="BoundedPareto"):
				axes.set_ylim([0.8,1.65])
				axes.set_xlim([0,3700])
				axes.xaxis.set_major_locator(MultipleLocator(1000))
				axes.xaxis.set_minor_locator(MultipleLocator(200))
				axes.yaxis.set_major_locator(MultipleLocator(0.2))
				axes.yaxis.set_minor_locator(MultipleLocator(0.04))
				plt.legend(loc='best', frameon=False, fontsize=14.5)
			if (d=="Gamma"):
				axes.set_ylim([0.925,1.525])
				axes.set_xlim([0,3700])
				axes.xaxis.set_major_locator(MultipleLocator(1000))
				axes.xaxis.set_minor_locator(MultipleLocator(200))
				axes.yaxis.set_major_locator(MultipleLocator(0.2))
				axes.yaxis.set_minor_locator(MultipleLocator(0.04))
				plt.legend(loc='best', frameon=False, fontsize=16)
			if (d=="TruncatedNormal"):
				axes.set_xlim([0,3700])
				axes.set_ylim([0.9,1.4])
				axes.xaxis.set_major_locator(MultipleLocator(1000))
				axes.xaxis.set_minor_locator(MultipleLocator(200))
				axes.yaxis.set_major_locator(MultipleLocator(0.2))
				axes.yaxis.set_minor_locator(MultipleLocator(0.04))
				plt.legend(loc='best', frameon=False, fontsize=16)
			if (d=="Uniform"):
				axes.set_xlim([0,3700])
				axes.set_ylim([0.9,1.775])
				axes.xaxis.set_major_locator(MultipleLocator(1000))
				axes.xaxis.set_minor_locator(MultipleLocator(200))
				axes.yaxis.set_major_locator(MultipleLocator(0.2))
				axes.yaxis.set_minor_locator(MultipleLocator(0.04))
				plt.legend(loc='best', frameon=False, fontsize=16)
			if (d=="Lognormal"):
				axes.set_xlim([0,3700])
				axes.set_ylim([0.9,1.35])
				axes.xaxis.set_major_locator(MultipleLocator(1000))
				axes.xaxis.set_minor_locator(MultipleLocator(200))
				axes.yaxis.set_major_locator(MultipleLocator(0.2))
				axes.yaxis.set_minor_locator(MultipleLocator(0.04))
				plt.legend(loc='best', frameon=False, fontsize=16)
			if (d=="Weibull"):
				axes.set_xlim([0,3700])
				axes.set_ylim([0.9,2])
				axes.xaxis.set_major_locator(MultipleLocator(1000))
				axes.xaxis.set_minor_locator(MultipleLocator(200))
				axes.yaxis.set_major_locator(MultipleLocator(0.2))
				axes.yaxis.set_minor_locator(MultipleLocator(0.04))
				plt.legend(loc='best', frameon=False, fontsize=16)
			if (d=="Pareto"):
				axes.set_xlim([0,3700])
				axes.set_ylim([0.9,1.45])
				axes.xaxis.set_major_locator(MultipleLocator(1000))
				axes.xaxis.set_minor_locator(MultipleLocator(200))
				axes.yaxis.set_major_locator(MultipleLocator(0.2))
				axes.yaxis.set_minor_locator(MultipleLocator(0.04))
				plt.legend(loc='best', frameon=False, fontsize=16)

			
			plt.tight_layout()
			plt.tick_params(axis='both', which='major', labelsize=21)
			plt.savefig(path_+"plot_value_R_C_"+d.lower()+".pdf",orientation='landscape')
			plt.close()



	settings.beta=0.0
####################### END simulation impact of R and C #######################










####################### START Simulation Scenario 2 , algorithm's performance when mean and stdev varies for application ON_MR_segmentation_v2 (lognormal fit) #######################
if (settings.generates_HPC_simulation_data_ON_MR_segmentation_v2 or settings.generates_HPC_plots_from_data_ON_MR_segmentation_v2):
	
	
	wtF = [linear]#,linear_interpolation_409]#,quadratic_interpolation_409]
	invwtF = [invlinear]#,invlinear_interpolation_409]#,quadratic_interpolation_409]
	wtF_str=["cloud_scenario"]#,"hpc_scenario"]#,"quadratic"]


	# vect_mean = [i for i in range(1,nb_iterations+1,1)]

	vect_mean = [0.1,0.3,0.5,0.7,0.9,1,3,5,7,9,10]
	# vect_mean = [0.4*i for i in range(1,nb_iterations+1,1)]
	vect_stdev = [0.1,0.3,0.5,0.7,0.9,1,3,5,7,9,10] #[i for i in range(1,nb_iterations+1,1)]

	nb_iterations = len(vect_mean) #15

	## compute the optimal cost for the different mean values  (used for normalization later) with different WT function
	assert (len(wtF)==len(invwtF)==len(wtF_str))


	epsilon = 1.0 #epsilon used in FPTAS bounded
	distrib = "Lognormal"
	
	ckpt_cost_list = [600,1200,3600,7200,43200,86400]
	

	# chose the value of beta (which cost function is used)
	settings.beta = 1.0
	
	path_= path+"beta="+str(int(settings.beta))+"/scenario2/"

	### Generates the data 
	if (settings.generates_HPC_simulation_data_ON_MR_segmentation_v2):
		for ckpt_cost in ckpt_cost_list:


			print("********************** ckpt cost = "+str(ckpt_cost)+" **********************\n\n")

			result = []
			
			for j in range (0,len(wtF)):
				print("Generating data of algorithms for waiting time function "+str(wtF_str[j]))
				r =[]
				
				


				settings.C = settings.R = ckpt_cost/(3600*24)

				nockpt,allckpt,perf,periodic_ac,periodic_nc=[],[],[],[],[]

				## Cost of Algorithms : FPTAS bounded and variants, periodic and variant
				for mu in vect_mean:
					print("				     Mean --> mu = "+str(mu)+"mu")

					# fixed stdev
					stdev = 70962.57928201828/(3600*24)
					# varying mean
					distri.mean_lognormal = (mu*77038.73080270347)/(3600*24)
					distri.variance_lognormal = pow(stdev,2)
				
					distri.nu_lognormal =  log(getattr(distri,"mean_lognormal")/sqrt(pow(stdev/getattr(distri,"mean_lognormal"),2)+1))
					distri.kappa_lognormal = sqrt(log(pow(stdev/getattr(distri,"mean_lognormal"),2)+1)) 
					distri.min_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*0.00001-1.0)*getattr(distri,"kappa_lognormal")*sqrt(2.0)))
					settings.max_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*0.99999-1.0)*getattr(distri,"kappa_lognormal")*sqrt(2.0)))
					
										
					#cost of optimal offline solution
					omniscient = (wtF[j](distri.mean_lognormal) + settings.beta*distri.mean_lognormal)

				
					print("					 - FPTAS for distribution "+str(distrib))
					n,res,sched = FPTAS_bounded(distrib, wtF[j],epsilon)
					print(res)
					print("					 ... done")
					
					# FPTAS nockpt
					print("					 - No checkpoint result for distribution "+str(distrib))
					n,nocheckpt,liste = FPTAS_bounded(distrib, wtF[j], epsilon, False, True)
					print(nocheckpt)
					print("					 ... done")
					
					# FPTAS allckpt
					print("					 - FPTAS for distribution "+str(distrib)+" in allCheckpoint")
					n,res_ac,sched_ac = FPTAS_bounded(distrib, wtF[j], epsilon, True, False)
					print(res_ac)
					print("					 ... done")

					# print the normalized results
					print("				    --> res = "+str(res))
					print("				    --> res (by NO-Chkpt) = "+str(res/nocheckpt))
					print("				    --> res (by omniscient) = "+str(res/omniscient))
					print("				    --> res (by ALL-Chkpt) = "+str(res/res_ac)+"\n\n")
					nockpt.append(nocheckpt/omniscient)
					allckpt.append(res_ac/omniscient)
					perf.append(res/omniscient)	


					# periodic
					T_candidates = [i for i in range(1,51)]
					min_ = float('inf')
					for T in T_candidates:
						res,sched = periodic(distrib, wtF[j], T)
						if (res<min_):
							min_ = deepcopy(res)
					periodic_ac.append(min_/omniscient)
					
					# periodic no ckpt
					min_ = float('inf')
					for T in T_candidates:
						res,sched = periodic(distrib, wtF[j], T, False)
						if (res<min_):
							min_ = deepcopy(res)
					periodic_nc.append(min_/omniscient)

				r.append(perf)
				r.append(allckpt)
				r.append(nockpt)
				r.append(periodic_ac)
				r.append(periodic_nc)
				result.append(r)
				
			assert (len(perf)==len(nockpt)==len(vect_mean))
			with open(path_+"plot_mean_"+str(int(settings.C*3600*24))+".txt", 'wb') as filehandle:
				# store the data as binary data stream
				pickle.dump(result, filehandle)


			### compute the performances for other algorithms when stand. dev. varies
			print("\n\n")

			result = []
		

			for j in range (0,len(wtF)):
				print("Generating data of algorithms for Intrepid interpolation, "+str(wtF_str[j])+" 409 WT on distribution 2 with different mean")
				r =[]
				
				nockpt,allckpt,perf,periodic_ac,periodic_nc=[],[],[],[],[]

				## Cost of Algorithms : FPTAS bounded and variants, periodic and variant
				for mu in vect_stdev:
					print("				     Standard derivation --> sigma = "+str(mu)+"sigma")
					
					
					settings.C = settings.R = ckpt_cost/(3600*24)

					# varying stdev
					stdev = (mu*70962.57928201828)/(3600*24)
					# fixed mean
					distri.mean_lognormal = 77038.73080270347/(3600*24)
					distri.variance_lognormal = pow(stdev,2)
				
					distri.nu_lognormal =  log(getattr(distri,"mean_lognormal")/sqrt(pow(stdev/getattr(distri,"mean_lognormal"),2)+1)) #(mu*1253.370)/(getattr(settings,"normalization_time"))
					distri.kappa_lognormal = sqrt(log(pow(stdev/getattr(distri,"mean_lognormal"),2)+1)) #pow(258.261/(getattr(settings,"normalization_time")),2)
					distri.min_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*0.001-1.0)*getattr(distri,"kappa_lognormal")*sqrt(2.0)))
					settings.max_lognormal = exp(getattr(distri,"nu_lognormal")+(erfinv(2.0*0.999-1.0)*getattr(distri,"kappa_lognormal")*sqrt(2.0)))
				

					# cost of optimal offline solution
					omniscient = wtF[j](distri.mean_lognormal) + settings.beta*distri.mean_lognormal
					

					print("					 - FPTAS for distribution "+str(distrib))
					n,res,sched = FPTAS_bounded(distrib, wtF[j],epsilon)
					print(res)
					print("					 ... done")
					
					# FPTAS nockpt
					print("					 - No checkpoint result for distribution "+str(distrib))
					n,nocheckpt,liste = FPTAS_bounded(distrib, wtF[j], epsilon, False, True)
					print(nocheckpt)
					print("					 ... done")
					
					# FPTAS allckpt
					print("					 - FPTAS for distribution "+str(distrib)+" in allCheckpoint")
					n,res_ac,sched_ac = FPTAS_bounded(distrib, wtF[j], epsilon, True, False)
					print(res_ac)
					print("					 ... done")

					# print the normalized results
					print("				    --> res = "+str(res))
					print("				    --> res (by NO-Chkpt) = "+str(res/nocheckpt))
					print("				    --> res (by omniscient) = "+str(res/omniscient))
					print("				    --> res (by ALL-Chkpt) = "+str(res/res_ac)+"\n\n")
					nockpt.append(nocheckpt/omniscient)
					allckpt.append(res_ac/omniscient)
					perf.append(res/omniscient)	
					

					# periodic
					T_candidates = [i for i in range(1,51)]
					min_ = float('inf')
					for T in T_candidates:
						res,sched = periodic(distrib, wtF[j], T)
						if (res<min_):
							min_ = deepcopy(res)
					periodic_ac.append(min_/omniscient)
					
					# periodic no ckpt
					min_ = float('inf')
					for T in T_candidates:
						res,sched = periodic(distrib, wtF[j], T, False)
						if (res<min_):
							min_ = deepcopy(res)
					periodic_nc.append(min_/omniscient)

				r.append(perf)
				r.append(allckpt)
				r.append(nockpt)
				r.append(periodic_ac)
				r.append(periodic_nc)
				result.append(r)
				
			with open(path_+"plot_variance_"+str(int(settings.C*3600*24))+".txt", 'wb') as filehandle:
				# store the data as binary data stream
				pickle.dump(result, filehandle)






	### Plot the results for stdev and mean variation
	if (settings.generates_HPC_plots_from_data_ON_MR_segmentation_v2):
		

		for ckpt_cost in ckpt_cost_list:
			markers = ["x","*","s",">","<"]
			res = []
			res2 = []
			
			path_= path+"beta="+str(int(settings.beta))+"/scenario2/"
			with open(path_+"plot_mean_"+str(ckpt_cost)+".txt", 'rb') as filehandle:
				# read the data as binary data stream
				res = pickle.load(filehandle)
			with open(path_+"/plot_variance_"+str(ckpt_cost)+".txt", 'rb') as filehandle:
				# read the data as binary data stream
				res2 = pickle.load(filehandle)

			print(res)
			print(len(res[0][1]))
			print(len(vect_stdev))
			print("\n")
			print(res2)


			colors = ['black','darkorange','red','green','blue']

			print("\n\nGenerating plots...")
			algorithms_str = ['{\sc Dyn-Prog-Count}','{\sc All-Ckpt}','{\sc No-Ckpt}','{\sc All-Ckpt-Per}','{\sc No-Ckpt-Per}']
			assert(len(colors)==len(algorithms_str))					



			mean = 77038.73080270347
			stdev = 70962.57928201828

			norm_time = 3600*24 # shift to days


			# plots
			for j in range(0,len(wtF)):
				maxi1 = -1
				maxi2 = -1
				

				##### when mean varies #####
				plt.figure()
				ax = plt.gca()

				ax.loglog()
				plt.gca().yaxis.set_major_locator(FixedLocator([0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0]))
				plt.gca().yaxis.set_major_formatter(ScalarFormatter())
			

				for i in range(0,len(res[j])):	
					#maxi = max(maxi, max(res[j][i]))
					maxi1 = max(maxi1,max([res[j][i][k] for k in range(0,len(vect_mean))]))
					maxi2 = max(maxi2,max([res2[j][i][k] for k in range(0,len(vect_stdev))]))
					# line, = ax.plot([(vect_mean[l]*mean)/norm_time for l in  range(0,len(vect_mean),2)], [res[j][i][k] for k in range(0,len(vect_mean),2)] , colors[i], marker=markers[i], lw=3, alpha=0.6, label=r''+algorithms_str[i])
					if (i==0):
						line, = ax.plot([vect_mean[l] for l in  range(0,len(vect_mean))], [res[j][i][k] for k in range(0,len(vect_mean))] , colors[i], marker=markers[i], lw=4, alpha=0.6, label=r''+algorithms_str[i],markersize=9)
					else:
						line, = ax.plot([vect_mean[l] for l in  range(0,len(vect_mean))], [res[j][i][k] for k in range(0,len(vect_mean))] , colors[i], marker=markers[i], lw=4, alpha=0.6, label=r''+algorithms_str[i],markersize=7)

				for axis in [ax.xaxis, ax.yaxis]:
					axis.set_minor_formatter(NullFormatter())
				ax.xaxis.set_major_formatter(ScalarFormatter())

				plt.legend(loc='best', frameon=False, fontsize=15)
				ax.set_xlabel(r''+'$\mu/\mu_o$',fontsize=24)
				ax.set_ylabel(r''+'{\sc Algo}/{\sc Omniscient}',fontsize=22)

				ax.tick_params(axis='both', which='major', labelsize=16)
				plt.axhline(y=1.0,linestyle='--')
				plt.tight_layout()
				plt.savefig(path_+wtF_str[j]+"_"+distrib.lower()+"_"+str(ckpt_cost)+"_mean.pdf",orientation='landscape')
				plt.close()





				##### when stdev varies #####
				plt.figure()
				ax = plt.gca()
				ax.loglog()
				
				plt.gca().yaxis.set_major_locator(FixedLocator([0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0]))
				plt.gca().yaxis.set_major_formatter(ScalarFormatter())

				
				for i in range(0,len(res2[j])):	
					line, = ax.plot([vect_stdev[l] for l in range(0,len(vect_stdev))], [res2[j][i][k] for k in range(0,len(vect_stdev))] , colors[i], marker=markers[i],lw=4, alpha=0.6, label=r''+algorithms_str[i],markersize=7)
				
			
				for axis in [ax.xaxis, ax.yaxis]:
					axis.set_minor_formatter(NullFormatter())
				ax.xaxis.set_major_formatter(ScalarFormatter())
				
				plt.legend(loc='best', frameon=False, fontsize=15)
				
				ax.set_ylabel(r''+'{\sc Algo}/{\sc Omniscient}',fontsize=24)
				ax.set_xlabel(r''+'$\sigma/\sigma_o$',fontsize=22,fontweight='bold')
				ax.tick_params(axis='both', which='major', labelsize=16)
				plt.axhline(y=1.0,linestyle='--')
				plt.tight_layout()
				plt.savefig(path_+wtF_str[j]+"_"+distrib.lower()+"_"+str(ckpt_cost)+"_stdev.pdf",orientation='landscape')
				plt.close()
	settings.beta=0.0
####################### END Simulation Scenario 2 #######################










print("\n\n\n!!!!!! END SIMULATIONS !!!!!!\n\n\n\n\n\n")



## END SIMULATION PROCESS ##