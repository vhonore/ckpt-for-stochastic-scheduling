import distri
from distri import *
import random


def init_settings():
    global R,C,alpha,beta,gamma_,normalization_time,simulation_FPTAS_bounded,simulation_FPTAS_bounded_plot_data,simulation_plot_FPTAS_bounded,simulation_periodic,simulation_generate_checkpointing_impact_data, simulation_plot_checkpointing_impact, generates_HPC_simulation_data_ON_MR_segmentation_v2,generates_HPC_plots_from_data_ON_MR_segmentation_v2, generates_HPC_simulation_data_ON_MR_segmentation_vDEV1, generates_HPC_plots_from_data_ON_MR_segmentation_vDEV1 ,plot_distrib, max_period #,nX,precision,nb_chunks,list_t,list_q, normalization_time
    R = 360/3600 # restarting cost in hours
    C = 360/3600 # checkpointing cost in hours
    alpha = 1.0 
    gamma_ = 0.0
    beta = 0
    normalization_time = 3600 # used to shift from second to hours in interpolations of HPC data
    max_period = 1000 # used to bound the minimum period we'll use: (b-a)/i for i in (1:max_period)
    simulation_FPTAS_bounded_plot_data = True
    simulation_plot_FPTAS_bounded =  True
    simulation_periodic = True
    simulation_generate_checkpointing_impact_data = True
    simulation_plot_checkpointing_impact = True
    generates_HPC_simulation_data_ON_MR_segmentation_v2 = True
    generates_HPC_plots_from_data_ON_MR_segmentation_v2 = True


    global pthres, pthres_b_FPTAS_bounded, max_exponential, max_weibull, max_gamma, max_lognormal, max_truncatednormal, max_pareto, max_uniform, max_beta, max_boundedpareto, max_laplace, epsilon_unbounded
    pthres = 0.9999999 ## threshold pmax value for discretization (the upperbound where we truncate)
    max_exponential = quant_exponential(pthres)
    # print(max_exponential)
    max_weibull = quant_weibull(pthres)
    # print(max_weibull)
    max_gamma = quant_gamma(pthres)
    # print(max_gamma)
    max_lognormal = quant_lognormal(pthres)
    # print(max_lognormal)
    max_truncatednormal = distri.b_truncatednormal
    # print(max_truncatednormal)
    max_pareto = quant_pareto(pthres)
    # print(max_pareto)
    max_uniform = distri.b_uniform
    # print(quant_uniform(pthres))
    max_beta = 1.0
    # print(quant_beta(pthres))
    max_boundedpareto = distri.H_boundedpareto
    # print(quant_boundedpareto(pthres))

    # simulation values for epsilon for FPTAS bounded and the different period values periods for periodic algorithm
    global epsilon,periods
    epsilon = [2,1.9,1.8,1.7,1.6,1.5,1.4,1.3,1.2,1.1,1,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1]
    periods = [i for i in range(1,max_period+1)]

    


########################## START Waiting Time Functions ##########################


############### Linear (cloud cost function if beta=0) ###############

# WT(t) = t --> alpha = 1.0, gamma = 0.0
def linear(time):
	return alpha*time + gamma_

def invlinear(time):
	return (time-gamma_)/alpha


