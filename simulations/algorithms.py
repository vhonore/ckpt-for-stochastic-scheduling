from copy import *
from cmath import *
import scipy
import os
from numpy import *
import settings

import distri
from tools import *





####### LIST OF ALGORITHMS HERE:
# dynamic_programming_discrete, optimal algorithm for discrete distribution
# FPTAS for bounded distributions (with ALL-CKPT and NO-CKPT versions)
# periodic algorithm (either with checkpoints or not)













########################## START Algorithms ##########################

# intermediate function used in dynamic_programming_discrete (dyn. prog of Theorem 2)
# distrib = [[vi,...],[fi...]]
def compute_fk(distrib):
	s = len(distrib[1])
	res = [0] * (s+1)
	for i in range(s-1, -1, -1):
		res[i] = distrib[1][i] + res[i+1]
	return res





# Theorem 2 
# distrib is the distribution name (string)
# distrib = [[],[]] list of v_i and list of fi
# waitingTimeFunction is the cost functions, must be implemented in settings.py
# numberChunks is the size of distrib
def dynamic_programming_discrete(distrib, distrib_name, waitingTimeFunction, numberChunks):
	assert(numberChunks>0)
	assert(len(distrib[0])==len(distrib[1])==numberChunks)
	
	assert(distrib_name in list_distrib)

	min_distrib = getattr(distri, "min_"+distrib_name.lower())
    

	# print(distrib)
	expt_t = []
	# assert(len(distrib[0])==numberChunks and len(distrib[1])==numberChunks)

	# expt_t saves the intermediate computations of Eckpt(_,_)
	expt_t = zeros((numberChunks+1, numberChunks+1)) ## expt_t contains the computed expectation Eckpt(_,_)


	# initialize Eckpt(_,n)
	for i in range (0,numberChunks+1):
		m = getattr(distri, "mean_"+distrib_name.lower())
		expt_t[i][numberChunks] =  getattr(settings, "beta")*m

	# opt_j keeps the value of j and delta_j at each step
	opt_j = zeros((numberChunks+1, numberChunks+1)) ## opt_j is used to backtrack the optimal sequence with checkpoint decisions ([opt_j,delta_j])
 	# opt_delta_j keeps the value of delta_j at each step
	opt_delta_j = zeros((numberChunks+1, numberChunks+1)) ## opt_delta_j is used to save checkpointing decisions
 

	# compute all the values of fk
	fk = compute_fk(distrib) 




	# complete the table bottom to up
	for il in range(numberChunks-1,-1,-1):
		for ic in range (il,-1,-1):
			# expt_t,opt_j,opt_delta_j = Eckpt(expt_t, opt_j, opt_delta_j, ic, il, distrib, distrib_name, fk, numberChunks, waitingTimeFunction)
			min_ = float('inf')
			j= il+1
			delta_j = 0
			for j_ in range(il+1,numberChunks+1): # for j in [il+1,numberChunks]
				fk_ =  fk[j_]
				# for i in range(j_,numberChunks): # summation of the fi for k=j+1 to n 
				# 	fk_+=distrib[1][i]
				
				tmp,candidate=0,0
				##### if (ic!=0)
				if (ic):
					# if delta_j_=0
					tmp =  expt_t[ic][j_] + (waitingTimeFunction(settings.R+(distrib[0][j_-1]-distrib[0][ic-1] ))+getattr(settings, "beta")*settings.R)*fk[il] + getattr(settings, "beta")*(distrib[0][j_-1]-distrib[0][ic-1] )*fk_
					# if delta_j_=1
					candidate =  expt_t[j_][j_] + (waitingTimeFunction(settings.R+(distrib[0][j_-1]-distrib[0][ic-1] )+settings.C)+getattr(settings, "beta")*settings.R)*fk[il] +getattr(settings, "beta")*settings.C*fk_
				##### if (ic>0)
				else:
					# if delta_j_=0
					tmp = expt_t[0][j_] + waitingTimeFunction(distrib[0][j_-1])*fk[il] + settings.beta*(distrib[0][j_-1])*fk_
					# if delta_j_=1
					candidate = expt_t[j_][j_] + waitingTimeFunction(distrib[0][j_-1]+settings.C)*fk[il] + settings.beta*settings.C*fk_
				# guess best expectation is when j candidate is checkpointed
				delta_j_ = 1
				# we look if the non checkpointed cost for j is better than the checkpointed one
				if (tmp<candidate):
					candidate=deepcopy(tmp)
					delta_j_=0
				# look for global minimum over the different j
				if (candidate<min_):
					j = deepcopy(j_)
					min_= deepcopy(candidate)
					delta_j = deepcopy(delta_j_)
			assert(min_!= float('inf'))
			expt_t[ic][il] = deepcopy(min_)
			opt_j[ic][il] = deepcopy(j)
			opt_delta_j[ic][il] = deepcopy(int(delta_j))

	
	# backtracking the sequence of indexes
	liste = []
	
	ic=0
	il =0
	delta_j= int(opt_delta_j[ic][il])

	while(il<numberChunks):
		# print(il,numberChunks)
		# print(opt_j[ic][il],delta_j)
		liste.append([int(opt_j[ic][il]),delta_j])
		if(delta_j):
			ic=int(deepcopy(il))
		il = int(opt_j[ic][il])
		delta_j = int(opt_delta_j[ic][il])
	# # print("\n\n\n")
	# print(liste)


	result = [[min_distrib,0]]
	for i in range(0,len(liste)):
		result.append([distrib[0][liste[i][0]-1],liste[i][1]])
	# print(opt_j)
	# print(liste)
	# print(expt_t)
	#return (expt_t[0][0],liste)
	
	# exit(-1)

	result[len(result)-1][1]=0
	print(result)
	# print(expt_t)
	return expt_t[0][0],result





# ALL-CKPT variant of Theorem 2 
# distrib is the distribution name (string)
# distrib = [[],[]] list of v_i and list of fi
# waitingTimeFunction is the cost functions, must be implemented in settings.py
# numberChunks is the size of distrib
def dynamic_programming_discrete_allcheckpoint(distrib, distrib_name, waitingTimeFunction, numberChunks):
	min_distrib = getattr(distri, "min_"+distrib_name.lower())#globals()["min_"+distrib.lower()]  
	mean_distrib = getattr(distri, "mean_"+distrib_name.lower())
	

	expt_t = []
	assert( len(distrib[0])==numberChunks and len(distrib[1])==numberChunks)
	assert(numberChunks)
	
	n = numberChunks

	# compute all the values of fk
	fk = compute_fk(distrib) 

	# tables to save expectation and indexes for backtracking
	expt_t = zeros(numberChunks+1)
	jstar = zeros(numberChunks+1)

	
	# initialization
	tim = settings.beta*mean_distrib
	expt_t[n] = tim

	# main loop
	for i in range(n-1,-1,-1): 
		R_ = settings.R
		if (i==0):
			R_=0

		min_=float('inf')
		j = -1
		for j_ in range (i+1,n+1):
			vj = distrib[0][j_-1]
			vi = 0
			if (i!=0):
				vi = distrib[0][i-1]
			C_ = settings.C
			if (j_==numberChunks):
				C_ = 0
			candidate = expt_t[j_] + settings.beta*settings.C*fk[j_-1] + (waitingTimeFunction(R_ + (vj-vi)+C_) + settings.beta*R_)*fk[i]
			if (candidate<min_):
				min_=deepcopy(candidate)
				j = deepcopy(j_)
		expt_t[i]=deepcopy(min_)
		jstar[i]=deepcopy(j)
		assert(j>=0)
		assert(min_!=float('inf'))
	

	# backtracking
	liste = []
	current_ind = int(jstar[0])
	while(current_ind<n):
		liste.append(current_ind)
		current_ind = int(jstar[current_ind])
	liste.append(n)

	#print([distrib[0][i] for i in liste])
	
	result = [[min_distrib,0]]
	for i in liste:
		result.append([distrib[0][i-1],1])
	result[len(result)-1][1]=0
	print(result)
	return expt_t[0],result






# NO-CKPT variant of Theorem 2 
# distrib is the distribution name (string)
# distrib = [[],[]] list of v_i and list of fi
# waitingTimeFunction is the cost functions, must be implemented in settings.py
# numberChunks is the size of distrib
def dynamic_programming_discrete_nocheckpoint(distrib, distrib_name, waitingTimeFunction, numberChunks):

	min_distrib = getattr(distri, "min_"+distrib_name.lower())#globals()["min_"+distrib.lower()]  

	
	expt_t = []
	assert( len(distrib[0])==numberChunks and len(distrib[1])==numberChunks)
	assert(numberChunks)
	
	n = numberChunks-1



	expt_t = []
	tim = waitingTimeFunction(distrib[0][n])
	tim += settings.beta*distrib[0][n]
	expt_t.append(tim)
	
	jstar = [n]
	for i in range(n-1,-1,-1): 
		opt_j = 0.0
		t = waitingTimeFunction(distrib[0][i])
		sum_adj_proba = 0.0
		for l in range(i,n+1):
			sum_adj_proba = sum_adj_proba + distrib[1][l]
		if(not sum_adj_proba): ## added by valentin
			sum_adj_proba = 1.0
		t = t + (distrib[1][i]/sum_adj_proba)*(settings.beta*distrib[0][i])
		cpt = 0.0
		for k in range(i+1,n+1):
			cpt = cpt +(distrib[1][k]/sum_adj_proba)
		t = t + (cpt *(settings.beta*distrib[0][i] + expt_t[n-i-1]))
		opt_j = deepcopy(i)
		for j in range(i+1,n+1):
			r = 0.0
			r  = waitingTimeFunction(distrib[0][j])
			for k in range(i,j+1):
				r = r + (distrib[1][k]/sum_adj_proba)*settings.beta*distrib[0][k]
			cpt = 0.0
			if(j!=n):
				for k in range(j+1,n+1):
					cpt = cpt + (distrib[1][k]/sum_adj_proba)
				r = r + (cpt *(settings.beta*distrib[0][j] + expt_t[n-j-1])) 
			if (r<t):
				opt_j = deepcopy(j)
				t = deepcopy(r)
		jstar.append(opt_j)
		expt_t.append(t)
	
 	# backtracking
	jstar = jstar[::-1]
	expt_t = expt_t[::-1]

	# exit(-1)
	liste = []
	current_ind = jstar[0]
	while(current_ind<n):
		liste.append(current_ind)
		current_ind = jstar[current_ind+1]
	liste.append(n)
	# print(jstar)

	#print([distrib[0][i] for i in liste])
	
	result = [[min_distrib,0]]
	for i in liste:
		result.append([distrib[0][i],0])
	
	print(result)
	return expt_t[0],result






# DYN-PROG-COUNT algorithm (Algorithm 1)
# distrib is the distribution name (string)
# waitingTimeFunction is the cost functions, must be implemented in settings.py
# epsilon is the precision parameter
# allCheckpoint to run ALL-CKPT variant, noCheckpoint to run NO-CKPT variant
def FPTAS_bounded(distrib, waitingTimeFunction, epsilon, allCheckpoint=False, noCheckpoint=False):
	
	assert(distrib in getattr(settings, "list_distrib")) ## either it is a bounded distribution, either this is a call from FPTAS_unbounded
	 
	cpy = 0
	a =  getattr(distri, "min_"+distrib.lower())
	b =  getattr(settings, "max_"+distrib.lower())

	assert(not (allCheckpoint and noCheckpoint))

	# for those distributions, we have to adapt the max of the support. With original pthres, it's too big and we obtain a tremoundous amount of chunks. 
	# we reset temporarily pthres=0.9999 and compute n with b=Q(0.999)
	if (distrib in ("Pareto", "Weibull")):
		p = settings.pthres_b_FPTAS_bounded
		if (distrib=="Weibull"):
			p = 0.995
		cpy = deepcopy(getattr(settings, "max_"+distrib.lower())) # keep the old value
		b = getattr(distri, "quant_"+distrib.lower())(p)

	alp = getattr(settings, "alpha")
	bet = getattr(settings, "beta")
	gam = getattr(settings, "gamma_")
	
	## adapt the value of a in line 2 in min(a,R,C) to some distributions to have reasonnable number of chunks
	div = 100
	if (distrib=="Lognormal"):
		div=15
	if(distrib=="Weibull"):
		div=5
	if (settings.beta==1):
		if (distrib=="Pareto"):
			div = 10
		if (distrib=="BoundedPareto"):
			div = 10

	
	#var corresponds to a in  min(A,R,C) line 2, basically it is E(X)/100
	var = getattr(distri, "mean_"+distrib.lower())/div 
	tmp = 1/min(var,getattr(settings, "R"),getattr(settings, "C"))
	if (gam!=0):
		tmp = min(tmp,(alp+bet)/gam)
	cte = (b-a)*tmp
	n = int(ceil(cte/epsilon))
	print("    -> number chunks = "+str(n))
	assert(n)

	# Put back correct max value of support for distribution if concerned
	if (distrib=="Pareto"):
		max_pareto = deepcopy(cpy)
		# print(max_pareto)
	# elif(distrib=="Lognormal"):
	# 	max_lognormal = deepcopy(cpy)
	# 	# print(max_lognormal)
	elif(distrib=="Weibull"):
		max_weibull = deepcopy(cpy)
		print(max_weibull)
	

	# discretize the distribution
	# returns a distrib of size n, containing the first reservation equals to the minimum of the support
	distrib_d = discretization_proba(distrib, n-1)

	# def mean_(distrib_d):
	# r = 0
	# print(len(distrib_d[0]))
	# for i in range(0,len(distrib_d[0])):
	# 	r = r + distrib_d[0][i]*distrib_d[1][i]
	# return r


	# m = mean_(distrib_d)
	# print("mean continuous = "+str(m))
	# print("mean discrete = "+str(getattr(distri, "mean_"+distrib.lower())))


	res,sched = 0,[]

	if (not allCheckpoint and not noCheckpoint):
		res,sched = dynamic_programming_discrete(distrib_d, distrib, waitingTimeFunction, n)
	elif (allCheckpoint):
		res,sched = dynamic_programming_discrete_allcheckpoint(distrib_d, distrib, waitingTimeFunction, n)
	else:
		assert(noCheckpoint and not allCheckpoint)
		res,sched=	dynamic_programming_discrete_nocheckpoint(distrib_d, distrib, waitingTimeFunction, n)

	return n,res,sched





# ALL-CKPT-PER (Algorithm 2)
# distrib is the distribution name (string)
# waitingTimeFunction is the cost functions, must be implemented in settings.py
# period is the period T used for periodic algorithm
# withCheckpoint=False used to run the version without any checkpoint (NO-CKPT-PER)
def periodic(distrib, waitingTimeFunction, period, withCheckpoint=True):
	
	assert(distrib in getattr(settings, "list_distrib")) ## either it is a bounded distribution, either this is a call from FPTAS_unbounded
	
	cpy=0
	a =  getattr(distri, "min_"+distrib.lower())
	b =  getattr(settings, "max_"+distrib.lower())
	
	# if (distrib in ("Pareto", "Lognormal", "Weibull")):
	# 	cpy = deepcopy(getattr(settings, "max_"+distrib.lower())) # keep the old value
	# 	b = getattr(distri, "quant_"+distrib.lower())(settings.pthres_b_FPTAS_bounded)

	decision=int(withCheckpoint)
	# print(decision)

	sched = [[a,0]]

	# print(int(floor(period/(b-a))))
	#for i in range(1,int(floor(period/(b-a)))):
	t=a
	i=1
	liste = [i for i in range(1,period+1)]#int(floor(period)/(b-a))+1)]
	# print(liste)
	# print(period)
	for i in liste:
	# while(t<b):
		t = a + i*((b-a)/period)
		sched.append([t,decision])
		# if (distrib=="Uniform"):
		# 	print(t)
		i+=1
	# sched.append([b,0])
	sched[len(sched)-1][1]=0
	# print(sched)
	# print(cost_computation_stochastic(distrib,sched,waitingTimeFunction))
	# print(cost_computation_discrete(distrib,sched,waitingTimeFunction))
	
	# Put back correct max value of support for distribution if concerned
	# if (distrib=="Pareto"):
	# 	max_pareto = deepcopy(cpy)
	# 	# print(max_pareto)
	# elif(distrib=="Lognormal"):
	# 	max_lognormal = deepcopy(cpy)
	# 	# print(max_lognormal)
	# elif(distrib=="Weibull"):
	# 	max_weibull = deepcopy(cpy)
	# 	# print(max_weibull)

	return cost_computation_stochastic(distrib,sched,waitingTimeFunction),sched
	
	




########################## END Algorithms ##########################




















