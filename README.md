# Copyright (c) 2019-2022 Univ. Bordeaux #




# High Performance Reservation and Checkpointing Strategies for Stochastic Applications # 

This README file aims at presenting how to generate the simulation data of Inria RR-9294






### Getting Started ###

This repository contains all the code to reproduce the results obtained in Inria Research Report n° 9294. For convenience, the pdf of the RR is provided
(see RR-9294.pdf)

To run the code, a makefile is provided. Simple follow next steps to run and customize your tests.

Any question or issue can be adressed to valentin.honore@inria.fr






### Prerequisites ###

You will need to have python3 installed on your machine. The basic installation command would be like

```
sudo apt-get install python3-all
```

on mainstream Unix distributions.






### Running the simulations ###

Use the makefile at the root of the repository.

To run the tests:

```
cd simulations ; make
```

This run the main.py file with the provided configuration. Please refer to next section of this readme to personnalize the tests. All results are directly stored in a subdirectory of directory ```results```






### Customizing your tests ###



Firstly, the code is tuned to generate the paper results. However, you can change the parameters at your convenience. We briefly explain here the structure of our programs and the main parameters you can change in each file.



###### Files ######

The simulator is divided into 5 files:

* 'main.py': main file that executes all the tests.
* 'distri.py': contains the distribution descriptions and features
* 'algorithms.py': constains the code of heuristics we considered in the paper
* 'settings.py': contains the main parameters of the simulator, User can choose the tests he wants to run.
* 'tools.py': contains various range of useful functions



###### 'settings.py' ######

In this file, you can change the following parameters:

* 	 ```path=[results/]``` -- By default, all created files will be in ```./results/``` folder and subfolders already created. Do not change it to avoid subfolder conflicts.
*    ```alpha = 1.0``` -- Setup for cloud model (do not change)
*    ```gamma_ = 0.0``` -- Setup for cloud model (do not change)
*    ```beta = 0.0``` -- Cost function: WT(T)+ beta.min(X,T) (updated to 1.0 for HPC cost function)
*    ```normalization_time = 3600``` -- Used to shift from second to hours in interpolations of HPC data
* 	 ```max_period = 1000``` -- Used to bound the maximum period we'll use: (b-a)/i for i in (1:max_period)
*    ```pthres = 0.99999999``` -- Threshold pmax value for discretization (the upperbound where we truncate)
*    ```max_exponential=quant_exponential(pthres)``` -- Maximum request for Exponential distribution
*    ```max_weibull = quant_weibull(pthres)``` -- Maximum request for Weibull distribution
*    ```max_gamma = quant_gamma(pthres)``` -- Maximum request for Gamma distribution
*    ```max_lognormal = quant_lognormal(pthres)``` -- Maximum request for Lognormal distribution
*    ```max_truncatednormal = quant_truncatednormal(pthres)``` -- Maximum request for TruncatedNormal distribution
*    ```max_pareto = quant_pareto(pthres)``` -- Maximum request for Pareto distribution
*    ```max_uniform = b_uniform``` -- Maximum request for Uniform distribution
*    ```max_beta = 1.0``` -- Maximum request for Beta distribution
*    ```max_boundedpareto = H_boundedpareto``` -- Maximum request for BoundedPareto distribution
* 	 ```simulation_FPTAS_bounded_plot_data = True ``` -- If ```True```, generates the data of Fig.7-8 in research report. This test takes may be long depending on the setup.
*    ```simulation_plot_FPTAS_bounded ``` -- If ```True```, generates the Fig.7-8 of the research report from previous data.
*	 ```simulation_periodic = True ``` -- If ```True```, generates Tables 2,3,4,5 of the research report, studying periodic algorithm performance.
* 	 ```simulation_generate_checkpointing_impact_data =  True ``` -- Generates the data associated to Figures 5,6 of the research report.
*	 ```simulation_plot_checkpointing_impact = True ``` -- Generates the plots of Figures 5,6 with above data.
* 	 ```generates_HPC_simulation_data_ON_MR_segmentation_v2 =  True ``` -- Generates the data for Scenario 2 (Figures 9,10)
*	 ```generates_HPC_plots_from_data_ON_MR_segmentation_v2 =  True``` -- Generates the plots of Figures 9 and 10 with above data.
*	 ```epsilon = [2,1.9,1.8,1.7,1.6,1.5,1.4,1.3,1.2,1.1,1,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1]``` -- Values of epsilon we test for Algorithm 1 performance
*	 ```periods = [i for i in range(1,max_period+1)]``` -- Candidate periods we use for periodic algorithm (b-a/1 to b-a/max_period) 
 
We separated in each simulation process the data generation and the figure generation. Hence, once you have the data, you can plot it independently from the generation.


###### 'distri.py' ######

This file contains the definition of all distributions. You are free to provide your own distribution instanciations. However, we do not provide at the moment a way to add new distributions to the simulator.

Each distribution is represented by its parameters, a variance, a mean and a ```min_distrib``` variable that contains the minimum value of the support. We consider distributions defined on positive support values.(```min_distrib = maximum(0,min_support)```).

The file also contains the PDF, CDF and inverse CDF function definitions of all distributions, as so as the quantile function of each distribution.



###### 'algorithms.py' ######

This file contains the code of the different algorithms we considered in the paper. No parameters are defined in this file.




###### 'tools.py' ######

This file contains the code of some useful functions (discretization schemes, side functions etc). No parameters are defined in this file.



###### 'main.py' ######

This file contains the execution code of the simulator. Each section of the code represents a simulation process, as described in ```settings.py```.
